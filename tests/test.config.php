<?php
return [
    'modules'                 => [
        'Space10\Di',
    ],
    'module_listener_options' => [
        'module_paths'      => [
            'vendor',
        ],
        'config_glob_paths' => [
            __DIR__ . DIRECTORY_SEPARATOR . 'module.config.php',
        ],
    ],
];
