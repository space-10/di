<?php

namespace Space10Test\Di;

use Magento\Framework\Simplexml;
use Space10\Di\ServiceConfig;
use Zend\ServiceManager\ServiceManager;

class ServiceConfigTest extends \PHPUnit_Framework_TestCase
{
    protected $serviceManager;

    public function setUp()
    {
        $this->serviceManager = new ServiceManager();
    }

    protected function getXmlConfig($file)
    {
        $xmlConfig = new Simplexml\Config();
        $xmlConfig->loadFile(__DIR__ . DIRECTORY_SEPARATOR . '_files' . DIRECTORY_SEPARATOR . $file);
        return $xmlConfig;
    }

    public function constructorCastDataProvider()
    {
        return [
            'no cast'                   => ['SimpleClassNoCast', 'Bar'],
            'string cast'               => ['SimpleClassCastString', 'Bar'],
            'integer cast from "1971"'  => ['SimpleClassCastInt', 1971],
            'float cast from "19.71"'   => ['SimpleClassCastFloat', 19.71],
            'boolean cast from "0"'     => ['SimpleClassCastBooleanWithNumber0', false],
            'boolean cast from "1"'     => ['SimpleClassCastBooleanWithNumber1', true],
            'boolean cast from "no"'    => ['SimpleClassCastBooleanWithNo', false],
            'boolean cast from "yes"'   => ['SimpleClassCastBooleanWithYes', true],
            'boolean cast from "true"'  => ['SimpleClassCastBooleanWithTrue', true],
            'boolean cast from "false"' => ['SimpleClassCastBooleanWithFalse', false],
        ];
    }

    public function factoryInstanceNameDataProvider()
    {
        return [
            'Simple class with factory class'                         => ['SimpleClassWithFactoryClass', 'Space10Test\Di\TestAsset\SimpleClass'],
            'Simple class with autodiscover factory method'           => ['SimpleClassWithAutodiscoverFactoryMethod', 'Space10Test\Di\TestAsset\SimpleClass'],
            'Simple class with specified factory method'              => ['SimpleClassWithSpecifiedFactoryMethod', 'Space10Test\Di\TestAsset\SimpleClass'],
            'Simple class with factory class and autodiscover method' => ['SimpleClassWithFactoryClassAndAutodiscoverMethod', 'Space10Test\Di\TestAsset\SimpleClass'],
            'Simple class with factory class and specified method'    => ['SimpleClassWithFactoryClassAndSpecifiedMethod', 'Space10Test\Di\TestAsset\SimpleClass'],

        ];
    }

    public function testIfClassCanBeInvoked()
    {
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'di.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

        $obj1 = $this->serviceManager->get('Space10Test\Di\TestAsset\SimpleClass');

        $this->assertInstanceOf('Space10Test\Di\TestAsset\SimpleClass', $obj1);

        // check if instance is singleton
        $this->assertSame($obj1, $this->serviceManager->get('Space10Test\Di\TestAsset\SimpleClass'),
            'Instance is not a singleton.');
    }

    public function testIfInstanceIsSingleton()
    {
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'singleton.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

        $obj1 = $this->serviceManager->get('SimpleClassSingleton1');
        $obj2 = $this->serviceManager->get('SimpleClassSingleton1');
        $this->assertSame($obj1, $obj2,
            'Instances of "SimpleClassSingleton1" and "SimpleClassSingleton1" must be identical.');
    }

    public function testIfInstanceIsNotSingleton()
    {
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'singleton.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

        $obj1 = $this->serviceManager->get('Space10Test\Di\TestAsset\SimpleClass');
        $obj2 = $this->serviceManager->get('Space10Test\Di\TestAsset\SimpleClass');
        $this->assertNotSame($obj1, $obj2,
            'Instances of "SimpleClassSingleton1" and "SimpleClassSingleton2" must not be identical.');

        $obj1 = $this->serviceManager->get('SimpleClassSingleton1');
        $obj2 = $this->serviceManager->get('SimpleClassSingleton2');
        $this->assertNotSame($obj1, $obj2,
            'Instances of "SimpleClassSingleton1" and "SimpleClassSingleton2" must not be identical.');
    }

    public function testIfInstanceCanRetrievedByAlias()
    {
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'aliases.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

        $origInstance = $this->serviceManager->get('MySimpleClass');
        $obj1 = $this->serviceManager->get('SimpleAlias');
        $obj2 = $this->serviceManager->get('AnotherSimpleAlias');

        $this->assertInstanceOf('Space10Test\Di\TestAsset\SimpleClass', $obj1);
        $this->assertInstanceOf('Space10Test\Di\TestAsset\SimpleClass', $obj1);

        $this->assertSame($origInstance, $obj1);
        $this->assertSame($origInstance, $obj2);
    }

    /**
     * @dataProvider constructorCastDataProvider
     *
     * @param $instanceName
     * @param $expectedValue
     */
    public function testIfConstructorIsCasted($instanceName, $expectedValue)
    {
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'constructor.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

        $this->assertTrue($this->serviceManager->has($instanceName),
            'Instance with name "' . $instanceName . '" not found.');

        $obj1 = $this->serviceManager->get($instanceName);
        $this->assertSame($expectedValue, $obj1->getValue());
        try {
        } catch (\Exception $e) {
            do {
                echo $e->getMessage();
            } while (($e = $e->getPrevious()) !== null);
        }
    }

    /**
     * @dataProvider factoryInstanceNameDataProvider
     *
     * @param $instanceName
     * @param $expectedClass
     */
    public function testFactoryReturnsCorrectInstance($instanceName, $expectedClass)
    {
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'factory.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

        $this->assertTrue($this->serviceManager->has($instanceName), 'Expecting the class was set to service manager');

        $obj = $this->serviceManager->get($instanceName);
        $this->assertInstanceOf($expectedClass, $obj);
    }

    public function testIfAbstractFactoryIsRegisteredProperly()
    {
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'abstract-factory.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

        $obj1 = $this->serviceManager->get('SimpleClassInstance');
        $this->assertInstanceOf('Space10Test\Di\TestAsset\SimpleClass', $obj1);
        $obj2 = $this->serviceManager->get('SimpleClassOtherInstance');
        $this->assertInstanceOf('Space10Test\Di\TestAsset\SimpleClass', $obj2);

        $this->assertSame($obj1, $this->serviceManager->get('SimpleClassInstance'));
    }

    public function testIfParentIsMergedCorrectly()
    {
        $this->markTestSkipped('Not yet available.');
        $xmlConfig = $this->getXmlConfig('simple' . DIRECTORY_SEPARATOR . 'parent-inheritance.xml');
        $serviceConfig = new ServiceConfig($xmlConfig);
        $this->serviceManager = new ServiceManager();
        $serviceConfig->configureServiceManager($this->serviceManager);

    }
}
