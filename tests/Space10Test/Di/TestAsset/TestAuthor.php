<?php
namespace Space10Test\Di\TestAsset;

class TestAuthor
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TestAuthor
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     *
     * @return TestAuthor
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }
}
