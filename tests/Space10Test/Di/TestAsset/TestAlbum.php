<?php

namespace Space10Test\Di\TestAsset;

class TestAlbum
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $year;

    /**
     * @var TestSong[]
     */
    protected $songs;

    /**
     * @var TestAuthor
     */
    protected $author;

    /**
     * @param            $name
     * @param            $year
     * @param TestAuthor $author
     */
    public function __construct($name, $year, TestAuthor $author)
    {
        $this->name = $name;
        $this->year = $year;
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TestAlbum
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     *
     * @return TestAlbum
     */
    public function setYear($year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return TestSong[]
     */
    public function getSongs()
    {
        return $this->songs;
    }

    /**
     * @param TestSong[] $songs
     *
     * @return TestAlbum
     */
    public function setSongs($songs)
    {
        $this->songs = $songs;
        return $this;
    }

    /**
     * @return TestAuthor
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param TestAuthor $author
     *
     * @return TestAlbum
     */
    public function setAuthor(TestAuthor $author)
    {
        $this->author = $author;
        return $this;
    }
}
