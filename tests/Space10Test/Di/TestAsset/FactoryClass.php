<?php

namespace Space10Test\Di\TestAsset;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FactoryClass implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new SimpleClass();
    }

    public static function getInstance(ServiceLocatorInterface $serviceLocator)
    {
        return new SimpleClass();
    }

    public function createInstance(ServiceLocatorInterface $serviceLocator)
    {
        return new SimpleClass();
    }
}
