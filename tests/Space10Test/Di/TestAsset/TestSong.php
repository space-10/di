<?php
namespace Space10Test\Di\TestAsset;

class TestSong
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var float
     */
    protected $length;

    /**
     * @var TestAlbum
     */
    protected $album;

    /**
     * @param                     $name
     * @param                     $length
     * @param TestAlbum $album
     */
    public function __construct($name, $length, TestAlbum $album)
    {
        $this->name = $name;
        $this->length = $length;
        $this->album = $album;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TestSong
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param float $length
     *
     * @return TestSong
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return TestAlbum
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @param TestAlbum $album
     *
     * @return TestSong
     */
    public function setAlbum(TestAlbum $album)
    {
        $this->album = $album;
        return $this;
    }
}
