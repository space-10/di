<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 20.09.2015
 * Time: 23:15
 */

namespace Space10\Di;

use Magento\Framework\Simplexml;
use Space10\Di\Definition\ArgumentDefinition;
use Zend\Debug\Debug;
use Zend\EventManager\Event;
use Zend\ModuleManager\ModuleManagerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\AbstractOptions;
use Zend\Stdlib\ArrayUtils;

/**
 * Class Configuration
 * @package Space10\Di
 */
class Configuration extends AbstractOptions
{
    const DEFAULT_XML_CONFIG_PATH = 'config/di.xml';
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var ModuleManagerInterface
     */
    protected $moduleManager;

    /**
     * @var array
     */
    protected $configPaths;

    /**
     * @var string
     */
    protected $configCache;

    /**
     * @var bool
     */
    protected $generateFactories;

    /**
     * @var string
     */
    protected $factoryDir;

    /**
     * @var string
     */
    protected $factoryNamespace;

    protected $classes;

    protected static $calls = 0;

    /**
     * @param array $config
     * @param ModuleManagerInterface $moduleManager
     */
    public function __construct(array $config, ModuleManagerInterface $moduleManager)
    {
        parent::__construct($config);
        $this->moduleManager = $moduleManager;
        $this->classes = [];
    }

    public function getConfig()
    {
        $moduleManager = $this->moduleManager;
        $reader = new Simplexml\ConfigReader($moduleManager);
        $reader->getEventManager()->attach(Simplexml\ConfigReader::EVENT_MERGE, [$this, 'onConfigMerge']);
        $reader->getConfig($this->getConfigPaths());

        return $this->classes;
    }

    /**
     * @param Event $e
     * @return Simplexml\Config
     */
    public function onConfigMerge(Event $e)
    {
        /* @var $target Simplexml\Config */
        $target = $e->getParam('mergeTo');
        $files = $e->getParam('files');
        $files = array_unique($files);
        $aliases = [];
        foreach ($files as $file) {

            /**
             * @todo do caching here!
             */

            $prototype = new Simplexml\Config();
            if ($prototype->loadFile($file)) {
                $x = $prototype->getNode();
                foreach ($x->children() as $child) {
                    /* @var $child Simplexml\Element */
                    if ($child->getName() == 'class') {
                        $name = (string)$child->getAttribute('name');
                        $xmlDef = $child->asArray();
                        unset($xmlDef['@']['name']);

                        $renameList = [
                            'factory-class' => 'factory_class',
                            'factory-method' => 'factory_method',
//                            '' => '',

                        ];
                        $classDefinition = $xmlDef['@'];
                        foreach (array_keys($classDefinition) as $k) {
                            if (strpos($k, '-') !== false) {
                                $classDefinition[str_replace('-', '_', $k)] = $classDefinition[$k];
                                unset($classDefinition[$k]);
                            }
                        }

                        if (isset($xmlDef['constructor-arg'])) {
                            $constructorArguments = [];
                            foreach ($xmlDef['constructor-arg'] as $arg) {
                                $carg = $arg['@'];
                                // if reference is given there can be no value
                                if (isset($carg['ref']) && !empty($carg['ref'])) {
                                    $carg['value'] = $carg['ref'];
                                    $carg['type'] = ArgumentDefinition::VALUE_TYPE_OBJECT;
                                }

                                $constructorArguments[] = ['value' => $carg['value'], 'type' => $carg['type']];
                            }
                            $classDefinition['constructor'] = $constructorArguments;
                        }

                        if (isset($xmlDef['property'])) {
                            $properties = [];
                            foreach ($xmlDef['property'] as $arg) {
                                $carg = $arg['@'];

                                $position = null;
                                // if reference is given there can be no value
                                if (isset($carg['ref']) && !empty($carg['ref'])) {
                                    $carg['value'] = $carg['ref'];
                                    $carg['type'] = ArgumentDefinition::VALUE_TYPE_OBJECT;
                                }

                                // rename index to position
                                if (isset($carg['index']) && !empty($carg['index'])) {
                                    $position = $carg['index'];
                                }

                                $properties[$carg['name']] = [
                                    'value' => $carg['value'],
                                    'type' => $carg['type'],
                                    'method' => $carg['type'],
                                ];
                            }
                            $classDefinition['properties'] = $properties;
                        }

                        if (!isset($this->classes[$name])) {
                            $this->classes[$name] = $classDefinition;
                        } else {
                            $this->classes[$name] = ArrayUtils::merge($this->classes[$name], $classDefinition);
                        }
                    }

                    if ($child->getName() == 'alias') {
                        $alias = (string)$child->getAttribute('alias');
                        $classname = (string)$child->getAttribute('name');
                        $aliases[$classname][] = $alias;
                    }
                    $target->getNode()->appendChild($child);
                }
            }
        }

        foreach ($aliases as $class => $classAliases) {
            if (!isset($this->classes[$class]['aliases'])) {
                $this->classes[$class]['aliases'] = [];
            }
            $this->classes[$class]['aliases'] = ArrayUtils::merge($this->classes[$class]['aliases'], $classAliases);
        }

        return $target;
    }

    /**
     * @param Event $e
     * @return Simplexml\Config
     */
    public function readerMerge(Event $e)
    {
        /* @var $target Simplexml\Config */
        $target = $e->getParam('mergeTo');
        $files = $e->getParam('files');

        /* @var $classes \Magento\Framework\Simplexml\Element[] */
        $classes = [];
        $config = [];
        foreach ($files as $file) {
            $prototype = new Simplexml\Config();
            if ($prototype->loadFile($file)) {
                $config[] = $prototype;
                /* @var $xml \Magento\Framework\Simplexml\Config */
                $nodes = $prototype->getNode('class');
                if (!$nodes) {
                    continue;
                }

                foreach ($nodes as $clazz) {
                    /* @var $clazz \Magento\Framework\Simplexml\Element */

                    $name = $clazz->getAttribute('name');
                    if (isset($classes[$name])) {
                        $classes[$name]->extend($clazz, true);
                    } else {
                        $classes[$name] = $clazz;
                    }
                }
            }
        }

        /* add merged classes to $xmlConfig */
        foreach ($classes as $clazz) {
            $target->getNode()->appendChild($clazz);
        }

        /* add all other nodes (name != 'class') to $xmlConfig */
        foreach ($config as $xml) {
            /* @var $xml \Magento\Framework\Simplexml\Config */
            $nodes = $xml->getNode()->children();
            foreach ($nodes as $node) {
                /* @var $node \Magento\Framework\Simplexml\Element */
                if ($node->getName() == 'class') {
                    continue;
                }

                $target->getNode()->appendChild($node);
            }
        }

        return $target;
    }

    /**
     * @return array
     */
    public function getConfigPaths()
    {
        return $this->configPaths;
    }

    /**
     * @param array $configPaths
     * @return Configuration
     */
    public function setConfigPaths(array $configPaths)
    {
        $this->configPaths = array_unique($configPaths);

        return $this;
    }

    /**
     * @return string
     */
    public function getConfigCache()
    {
        return $this->configCache;
    }

    /**
     * @param string $configCache
     * @return Configuration
     */
    public function setConfigCache($configCache)
    {
        $this->configCache = $configCache;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isGenerateFactories()
    {
        return $this->generateFactories;
    }

    /**
     * @param boolean $generateFactories
     * @return Configuration
     */
    public function setGenerateFactories($generateFactories)
    {
        $this->generateFactories = $generateFactories;

        return $this;
    }

    /**
     * @return string
     */
    public function getFactoryDir()
    {
        return $this->factoryDir;
    }

    /**
     * @param string $factoryDir
     * @return Configuration
     */
    public function setFactoryDir($factoryDir)
    {
        $this->factoryDir = $factoryDir;

        return $this;
    }

    /**
     * @return string
     */
    public function getFactoryNamespace()
    {
        return $this->factoryNamespace;
    }

    /**
     * @param string $factoryNamespace
     * @return Configuration
     */
    public function setFactoryNamespace($factoryNamespace)
    {
        $this->factoryNamespace = $factoryNamespace;

        return $this;
    }

}