<?php
namespace Space10\Di;

use Magento\Framework\Simplexml;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Debug\Debug;

/**
 * Class ServiceConfigFactory
 * @package Space10\Di
 */
class ServiceConfigFactory implements FactoryInterface
{
    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed|ServiceConfig
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /* @var $configuration Configuration */
        $configuration = $serviceLocator->get(Configuration::class);
        $cfg = $configuration->getConfig();

        return new ServiceConfig($cfg);

        // return new ServiceConfig($this->getPreparedConfig($serviceLocator));
    }

    protected function getConfigFiles(ServiceLocatorInterface $serviceLocator)
    {
        /* @var $moduleManager \Zend\ModuleManager\ModuleManagerInterface */
        $moduleManager = $serviceLocator->get('ModuleManager');
        $serviceConfig = $serviceLocator->get('Config');

        $configPaths = [ServiceConfig::DEFAULT_CONFIG_PATH];

        if (isset($serviceConfig['space10di']) && isset($serviceConfig['space10di']['config_paths']) && is_array($serviceConfig['space10di']['config_paths'])) {
            $configPaths = array_merge($configPaths, $serviceConfig['space10di']['config_paths']);
        }

        if (
            isset($serviceConfig['space10']) &&
            isset($serviceConfig['space10']['di']) &&
            isset($serviceConfig['space10']['di']['config_paths']) &&
            is_array($serviceConfig['space10']['di']['config_paths'])
        ) {
            $configPaths = array_merge($configPaths, $serviceConfig['space10']['di']['config_paths']);
        }

        // remove duplicated paths from config
        $configPaths = array_unique($configPaths);

        $configFiles = [];
        $modules = $moduleManager->getLoadedModules(true);
        foreach ($modules as $moduleName => $module) {
            $moduleClass = new \ReflectionClass($module);
            $modulePath = dirname($moduleClass->getFileName());
            foreach ($configPaths as $relativePath) {
                $configFile = $modulePath . DIRECTORY_SEPARATOR . $relativePath;
                if (is_readable($configFile)) {
                    $configFiles[] = $configFile;
                }
            }
        }

        $config = [];
        foreach ($configFiles as $file) {
            $prototype = new Simplexml\Config();
            if ($prototype->loadFile($file)) {
                $config[] = $prototype;
            }
        }

        return $config;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return \Magento\Framework\Simplexml\Config
     * @todo move stuff to seperated class which extends from Simplexml\Config with own merge strategy
     */
    protected function getPreparedConfig(ServiceLocatorInterface $serviceLocator)
    {
        $config = $this->getConfigFiles($serviceLocator);

        $xmlConfig = new Simplexml\Config();
        $xmlConfig->loadString('<di/>');

        $classes = $this->getMergedClasses($config);

        /* add merged classes to $xmlConfig */
        foreach ($classes as $clazz) {
            $xmlConfig->getNode()->appendChild($clazz);
        }

        /* add all other nodes (name != 'class') to $xmlConfig */
        foreach ($config as $xml) {
            /* @var $xml \Magento\Framework\Simplexml\Config */
            $nodes = $xml->getNode()->children();
            foreach ($nodes as $node) {
                /* @var $node \Magento\Framework\Simplexml\Element */
                if ($node->getName() == 'class') {
                    continue;
                }

                $xmlConfig->getNode()->appendChild($node);
            }
        }
        return $xmlConfig;
    }

    /**
     * Merge class nodes by name attribute
     *
     * @param \Magento\Framework\Simplexml\Config[] $configs
     *
     * @return \Magento\Framework\Simplexml\Element[]
     */
    protected function getMergedClasses(array $configs)
    {
        /* @var $classes \Magento\Framework\Simplexml\Element[] */
        $classes = [];
        foreach ($configs as $moduleName => $xml) {
            /* @var $xml \Magento\Framework\Simplexml\Config */
            $nodes = $xml->getNode('class');
            if (!$nodes) {
                continue;
            }

            foreach ($nodes as $clazz) {
                /* @var $clazz \Magento\Framework\Simplexml\Element */

                $name = $clazz->getAttribute('name');
                if (isset($classes[$name])) {
                    $classes[$name]->extend($clazz, true);
                } else {
                    $classes[$name] = $clazz;
                }
            }
        }
        return $classes;
    }
}
