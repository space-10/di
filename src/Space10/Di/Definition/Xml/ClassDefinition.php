<?php

namespace Space10\Di\Definition\Xml;

use Magento\Framework\Simplexml;
use Magento\Framework\Stdlib\BooleanUtils;

/**
 * Class ClassDefinition
 * @package Space10\Di\Definition\Xml
 */
class ClassDefinition
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $classname;

    /**
     * @var string
     */
    protected $initMethod;

    /**
     * @var string
     */
    protected $destroyMethod;

    /**
     * @var string
     */
    protected $factoryClass;

    /**
     * @var string
     */
    protected $factoryMethod;

    /**
     * @var bool
     */
    protected $abstractFactory;

    /**
     * Should object be singleton. Default: true
     * @var bool
     */
    protected $singleton = true;

    /**
     * @var bool
     */
    protected $abstract;

    /**
     * @var string
     */
    protected $parent;

    /**
     * @var ClassArgumentDefinition[]
     */
    protected $constructorArgs;

    /**
     * @var ClassPropertyDefinition[]
     */
    protected $properties;

    /**
     * @var array
     */
    protected $dependencies;

    /**
     * @param Simplexml\Element $node
     * @param array $dependencies
     */
    public function __construct(Simplexml\Element $node, array $dependencies)
    {
        $this->name = (string)$node->getAttribute('name');

        $booleanUtils = new BooleanUtils();
        $this->classname = $node->getAttribute('classname') ? $node->getAttribute('classname') : $this->name;
        $this->abstractFactory = null !== $node->getAttribute('abstract-factory') ? $booleanUtils->toBoolean($node->getAttribute('abstract-factory')) : false;
        $this->singleton = null !== $node->getAttribute('singleton') ? $booleanUtils->toBoolean($node->getAttribute('singleton')) : true;
        $this->factoryClass = $node->getAttribute('factory-class');
        $this->factoryMethod = $node->getAttribute('factory-method');

        $this->dependencies = isset($dependencies[$this->name]) && !empty($dependencies[$this->name]) ? $dependencies[$this->name] : [];

        /*
         * Future stuff
         */
        $this->abstract = null !== $node->getAttribute('abstract') ? $booleanUtils->toBoolean($node->getAttribute('abstract')) : false;  // not yet used
        $this->initMethod = $node->getAttribute('init-method'); // not yet used
        $this->destroyMethod = $node->getAttribute('destroy-method'); // not yet used
        // if there is a parent than the parent should be merged into this class in a previous step
        $this->parent = $node->getAttribute('parent'); // not yet used
    }

    /**
     * @return bool
     */
    public function hasProperties()
    {
        return !empty($this->properties);
    }

    /**
     * @return bool
     */
    public function hasConstructorArgs()
    {
        return !empty($this->constructorArgs);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ClassDefinition
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getClassname()
    {
        return $this->classname;
    }

    /**
     * @param string $classname
     *
     * @return ClassDefinition
     */
    public function setClassname($classname)
    {
        $this->classname = $classname;
        return $this;
    }

    /**
     * @return string
     */
    public function getInitMethod()
    {
        return $this->initMethod;
    }

    /**
     * @param string $initMethod
     *
     * @return ClassDefinition
     */
    public function setInitMethod($initMethod)
    {
        $this->initMethod = $initMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function getDestroyMethod()
    {
        return $this->destroyMethod;
    }

    /**
     * @param string $destroyMethod
     *
     * @return ClassDefinition
     */
    public function setDestroyMethod($destroyMethod)
    {
        $this->destroyMethod = $destroyMethod;
        return $this;
    }

    /**
     * @return string
     */
    public function getFactoryClass()
    {
        return $this->factoryClass;
    }

    /**
     * @param string $factoryClass
     *
     * @return ClassDefinition
     */
    public function setFactoryClass($factoryClass)
    {
        $this->factoryClass = $factoryClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getFactoryMethod()
    {
        return $this->factoryMethod;
    }

    /**
     * @param string $factoryMethod
     *
     * @return ClassDefinition
     */
    public function setFactoryMethod($factoryMethod)
    {
        $this->factoryMethod = $factoryMethod;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAbstractFactory()
    {
        return $this->abstractFactory;
    }

    /**
     * @param boolean $abstractFactory
     *
     * @return ClassDefinition
     */
    public function setAbstractFactory($abstractFactory)
    {
        $this->abstractFactory = $abstractFactory;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSingleton()
    {
        return $this->singleton;
    }

    /**
     * @param boolean $singleton
     *
     * @return ClassDefinition
     */
    public function setSingleton($singleton)
    {
        $this->singleton = $singleton;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAbstract()
    {
        return $this->abstract;
    }

    /**
     * @param boolean $abstract
     *
     * @return ClassDefinition
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;
        return $this;
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param string $parent
     *
     * @return ClassDefinition
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return ClassPropertyDefinition[]
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param ClassPropertyDefinition[] $properties
     *
     * @return ClassDefinition
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @return ClassArgumentDefinition[]
     */
    public function getConstructorArgs()
    {
        return $this->constructorArgs;
    }

    /**
     * @param ClassArgumentDefinition[] $constructorArgs
     *
     * @return ClassDefinition
     */
    public function setConstructorArgs($constructorArgs)
    {
        $this->constructorArgs = $constructorArgs;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFactory()
    {
        return $this->getFactoryClass() || $this->getFactoryMethod() || $this->isAbstractFactory();
    }
}
