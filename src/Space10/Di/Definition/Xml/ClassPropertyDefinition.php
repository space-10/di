<?php
namespace Space10\Di\Definition\Xml;

use Magento\Framework\Simplexml;
use Space10\Di\Exception;

/**
 * Class ClassPropertyDefinition
 * @package Space10\Di\Definition\Xml
 */
class ClassPropertyDefinition extends AbstractParameterDefinition
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $value;

    /**
     * @param Simplexml\Element $node
     */
    public function __construct(Simplexml\Element $node)
    {
        $this->name = (string)$node->getAttribute('name');
        $argumentType = (string)$node->getAttribute('type');
        if ($argumentType && (!in_array($argumentType, [
                    static::VALUE_TYPE_STRING,
                    static::VALUE_TYPE_INT,
                    static::VALUE_TYPE_FLOAT,
                    static::VALUE_TYPE_BOOL,
                ]) && !class_exists($argumentType))
        ) {
            // argument type is set but is not a valid value
            throw new Exception\InvalidTypeException("Type '" . $argumentType . "' is not a valid type or class.");
        }

        $refClass = (string)$node->getAttribute('ref');
        $value = (string)$node->getAttribute('value');
        if ($refClass) {
            $this->type = static::TYPE_OBJECT;
            $this->valueType = $argumentType ? $argumentType : $refClass;
            $this->value = $this->formatParameterValue($value, $refClass, $argumentType);

            if (!$argumentType) {
                // $refClass could be a class which is already registered in ServiceManager
                // @todo get class dependencies here to check for $refClass
            }
        } elseif (null !== $value) {
            $this->type = static::TYPE_VALUE;
            $this->valueType = $argumentType ? $argumentType : static::VALUE_TYPE_STRING;
            $this->value = $this->formatParameterValue($value, $refClass, $argumentType);
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ClassPropertyDefinition
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return ClassPropertyDefinition
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return ClassPropertyDefinition
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
