<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 10.04.2015
 * Time: 10:58
 */

namespace Space10\Di\Definition\Xml;

use Magento\Framework\Stdlib\BooleanUtils;
use Space10\Di\Exception;

/**
 * Class AbstractParameterDefinition
 * @package Space10\Di\Definition\Xml
 */
abstract class AbstractParameterDefinition
{
    const TYPE_OBJECT = 0;

    const TYPE_VALUE = 1;

    const VALUE_TYPE_STRING = 'string';

    const VALUE_TYPE_INT = 'int';

    const VALUE_TYPE_FLOAT = 'float';

    const VALUE_TYPE_BOOL = 'bool';

    /**
     * @param $value
     * @param null $ref
     * @param null $type
     * @return bool|float|int|null
     */
    public function formatParameterValue($value, $ref = null, $type = null)
    {
        if ($type && (!in_array($type, [
                    static::VALUE_TYPE_STRING,
                    static::VALUE_TYPE_INT,
                    static::VALUE_TYPE_FLOAT,
                    static::VALUE_TYPE_BOOL,
                ]) && !class_exists($type))
        ) {
            // argument type is set but is not a valid value
            throw new Exception\InvalidTypeException("Type '" . $type . "' is not a valid type or class.");
        }

        if ($ref) {
            return $ref;
        } elseif (null !== $value) {
            $type = $type ? $type : static::VALUE_TYPE_STRING;
            if ($type == static::VALUE_TYPE_INT) {
                $value = (int)$value;
            } elseif ($type == static::VALUE_TYPE_BOOL) {
                $util = new BooleanUtils([true, 1, 'true', '1', 'yes'], [false, 0, 'false', '0', 'no']);
                $value = $util->toBoolean($value);
            } elseif ($type == static::VALUE_TYPE_FLOAT) {
                $value = (float)$value;
            }
        }

        return $value;
    }
}
