<?php

namespace Space10\Di\Definition\Xml;

use Magento\Framework\Simplexml;
use Space10\Di\Exception;

/**
 * Class ClassArgumentDefinition
 * @package Space10\Di\Definition\Xml
 */
class ClassArgumentDefinition extends AbstractParameterDefinition
{
    protected $index;

    protected $type;

    protected $ref;

    protected $value;

    protected $valueType;

    /**
     * @param Simplexml\Element $node
     */
    public function __construct(Simplexml\Element $node)
    {

        // $this->index = (int)$node->getAttribute('index');
        $argumentType = (string)$node->getAttribute('type');
        $refClass = (string)$node->getAttribute('ref');
        $value = (string)$node->getAttribute('value');

        if ($argumentType && (!in_array($argumentType, [static::VALUE_TYPE_STRING, static::VALUE_TYPE_INT, static::VALUE_TYPE_FLOAT, static::VALUE_TYPE_BOOL]) && !class_exists($argumentType))) {
            // argument type is set but is not a valid value
            throw new Exception\InvalidTypeException("Type '" . $argumentType . "' is not a valid type or class.");
        }

        if ($refClass) {
            $this->type = static::TYPE_OBJECT;
            $this->valueType = $argumentType ? $argumentType : $refClass;
            $this->value = $this->formatParameterValue($value, $refClass, $argumentType);

            if (!$argumentType) {
                // $refClass must be a class which is already registered in ServiceManager
                // @todo get class dependencies here to check for $refClass
            }
        } elseif (null !== $value) {
            $this->type = static::TYPE_VALUE;
            $this->valueType = $argumentType ? $argumentType : static::VALUE_TYPE_STRING;
            $this->value = $this->formatParameterValue($value, $refClass, $argumentType);
        }
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return ClassArgumentDefinition
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     *
     * @return ClassArgumentDefinition
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param mixed $index
     *
     * @return ClassArgumentDefinition
     */
    public function setIndex($index)
    {
        $this->index = (int)$index;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return ClassArgumentDefinition
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getValueType()
    {
        return $this->valueType;
    }

    /**
     * @param string $valueType
     *
     * @return ClassArgumentDefinition
     */
    public function setValueType($valueType)
    {
        $this->valueType = $valueType;
        return $this;
    }
}
