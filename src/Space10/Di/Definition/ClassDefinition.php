<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 20.09.2015
 * Time: 16:15
 */

namespace Space10\Di\Definition;


use Zend\Stdlib\AbstractOptions;

/**
 * Class ClassDefinition
 * @package Space10\Di\Definition
 */
class ClassDefinition extends AbstractOptions
{
    /**
     * @var string
     */
    protected $classname;

    /**
     * @var bool
     */
    protected $abstract = false;

    /**
     * @var bool
     */
    protected $abstractFactory = false;

    /**
     * @var bool
     */
    protected $initializer = false;

    /**
     * @var string
     */
    protected $factoryClass = null;

    /**
     * @var string
     */
    protected $factoryMethod = null;

    /**
     * @var string
     */
    protected $destroyMethod = null;

    /**
     * @var string
     */
    protected $initMethod = null;

    /**
     * @var string
     */
    protected $parent = null;

    /**
     * @var bool
     */
    protected $singleton = true;

    /**
     * @var array
     */
    protected $aliases = null;

    /**
     * @var ArgumentDefinition[]
     */
    protected $constructor = null;

    /**
     * @var ArgumentDefinition[]
     */
    protected $properties = null;

    public function __construct($name, $options = null) {
        $this->setClassname($name);
        parent::__construct($options);
    }

    /**
     * @return string
     */
    public function getClassname()
    {
        return $this->classname;
    }

    /**
     * @param $classname
     * @return $this
     */
    public function setClassname($classname)
    {
        $this->classname = $classname;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isAbstract()
    {
        return $this->abstract;
    }

    /**
     * @param $abstract
     * @return $this
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isAbstractFactory()
    {
        return $this->abstractFactory;
    }

    /**
     * @param $abstractFactory
     * @return $this
     */
    public function setAbstractFactory($abstractFactory)
    {
        $this->abstractFactory = $abstractFactory;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isInitializer()
    {
        return $this->initializer;
    }

    /**
     * @param boolean $initializer
     * @return ClassDefinition
     */
    public function setInitializer($initializer)
    {
        $this->initializer = $initializer;

        return $this;
    }

    /**
     * @return string
     */
    public function getFactoryClass()
    {
        return $this->factoryClass;
    }

    /**
     * @param $factoryClass
     * @return $this
     */
    public function setFactoryClass($factoryClass)
    {
        $this->factoryClass = $factoryClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getFactoryMethod()
    {
        return $this->factoryMethod;
    }

    /**
     * @param $factoryMethod
     * @return $this
     */
    public function setFactoryMethod($factoryMethod)
    {
        $this->factoryMethod = $factoryMethod;

        return $this;
    }

    /**
     * @return string
     */
    public function getDestroyMethod()
    {
        return $this->destroyMethod;
    }

    /**
     * @param $destroyMethod
     * @return $this
     */
    public function setDestroyMethod($destroyMethod)
    {
        $this->destroyMethod = $destroyMethod;

        return $this;
    }

    /**
     * @return string
     */
    public function getInitMethod()
    {
        return $this->initMethod;
    }

    /**
     * @param $initMethod
     * @return $this
     */
    public function setInitMethod($initMethod)
    {
        $this->initMethod = $initMethod;

        return $this;
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $parent
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSingleton()
    {
        return $this->singleton;
    }

    /**
     * @param $singleton
     * @return $this
     */
    public function setSingleton($singleton)
    {
        $this->singleton = (bool)$singleton;

        return $this;
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * @param array $aliases
     * @return $this
     */
    public function setAliases(array $aliases)
    {
        $this->aliases = $aliases;

        return $this;
    }

    /**
     * @return ArgumentDefinition[]
     */
    public function getConstructor()
    {
        return $this->constructor;
    }

    /**
     * @param array $constructor
     * @return $this
     */
    public function setConstructor(array $constructor)
    {
        $params = [];
        foreach ($constructor as $name => $options) {
            $params[] = new ArgumentDefinition($name, $options['value'], $options['type']);
        }

        $this->constructor = $params;

        return $this;
    }

    /**
     * @return ArgumentDefinition[]
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     * @return $this
     */
    public function setProperties(array $properties)
    {
        $params = [];
        foreach ($properties as $name => $options) {
            $params[] = new ArgumentDefinition($name, $options['value'], $options['type']);
        }
        $this->properties = $params;

        return $this;
    }

    /**
     * @return bool
     */
    public function isInvokable() {
        return !$this->isFactory() && !$this->getProperties() && !$this->getConstructor();
    }

    /**
     * @return bool
     */
    public function isFactory() {
        return null !== $this->getFactoryClass() || null !== $this->getFactoryMethod() || true === $this->isAbstractFactory();
    }

    /**
     * @return string
     */
    public function getInstanceClassname() {
        return $this->getFactoryClass() ? $this->getFactoryClass() : $this->getClassname();
    }

    /**
     * A factory which can handled by the default zend service manager itself
     *
     * @return bool
     */
    public function isSimpleFactory() {
        return $this->getFactoryClass() && ($this->getFactoryMethod() == 'createService' || !$this->getFactoryMethod()) && !$this->getProperties() && !$this->getConstructor();
    }
}