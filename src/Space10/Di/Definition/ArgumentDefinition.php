<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 20.09.2015
 * Time: 16:29
 */

namespace Space10\Di\Definition;

use Space10\Di\Exception;

use Magento\Framework\Stdlib\BooleanUtils;

/**
 * Class ArgumentDefinition
 * @package Space10\Di\Definition
 */
class ArgumentDefinition
{
    const VALUE_TYPE_BOOL = 'bool';

    const VALUE_TYPE_INT = 'int';

    const VALUE_TYPE_FLOAT = 'float';

    const VALUE_TYPE_STRING = 'string';

    const VALUE_TYPE_OBJECT = 'ref';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool|float|int|string|mixed
     */
    protected $value;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $position;

    /**
     * @param string $name
     * @param string $value
     * @param string $type
     * @param int $position
     */
    public function __construct($name, $value, $type, $position = null)
    {
        $this->name = $name;
        $validTypes = [
            static::VALUE_TYPE_BOOL,
            static::VALUE_TYPE_INT,
            static::VALUE_TYPE_FLOAT,
            static::VALUE_TYPE_STRING,
            static::VALUE_TYPE_OBJECT,
        ];
        if ($type && (!in_array($type, $validTypes))) {
            // argument type is set but is not a valid value
            throw new Exception\InvalidTypeException("Value-type '" . $type . "' must be one of: " . var_export($validTypes,
                    true));
        }
        $this->value = $this->castValue($value, $type);
        $this->type = $type;
        $this->position = null !== $position ? (int) $position : null;
    }

    /**
     * @param $value
     * @param $type
     * @return bool|float|int
     */
    protected function castValue($value, $type)
    {
        if (null !== $value) {
            $type = $type ? $type : static::VALUE_TYPE_STRING;
            if ($type == static::VALUE_TYPE_INT) {
                $value = (int)$value;
            } elseif ($type == static::VALUE_TYPE_BOOL) {
                $util = new BooleanUtils([true, 1, 'true', '1', 'yes'], [false, 0, 'false', '0', 'no']);
                $value = $util->toBoolean($value);
            } elseif ($type == static::VALUE_TYPE_FLOAT) {
                $value = (float)$value;
            }
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ArgumentDefinition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool|float|int|mixed|string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param bool|float|int|mixed|string $value
     * @return ArgumentDefinition
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ArgumentDefinition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return ArgumentDefinition
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }
}