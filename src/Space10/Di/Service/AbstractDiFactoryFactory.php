<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 20.09.2015
 * Time: 20:16
 */

namespace Space10\Di\Service;


use Magento\Framework\Stdlib\StringUtils;
use Space10\Di\Definition\ArgumentDefinition;
use Space10\Di\Definition\ClassDefinition;
use Space10\Di\Exception;
use Zend\Debug\Debug;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


/**
 * Class AbstractDiFactoryFactory
 * @package Space10\Di\Service
 */
class AbstractDiFactoryFactory implements FactoryInterface
{
    protected $classDefinition;

    public function __construct(ClassDefinition $class)
    {
        $this->classDefinition = $class;
    }

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $class = $this->classDefinition;
        // do we have properties or constructor arguments?
        $constructorArguments = [];
        if ($class->getConstructor()) {
            foreach ($class->getConstructor() as $arg) {
                $position = $arg->getPosition();
                if ($position && isset($constructorArguments[$position])) {
                    throw new Exception\DuplicatedIndexException('A constructor index must be unique.');
                }

                $value = $arg->getValue();
                if ($arg->getType() == ArgumentDefinition::VALUE_TYPE_OBJECT) {

                    if ($value != 'ServiceLocator') {
                        $value = $serviceLocator->get($value);
                    } else {
                        $value = $serviceLocator;
                    }
                }

                if ($position) {
                    $constructorArguments[$position] = $value;
                } else {
                    $constructorArguments[] = $value;
                }
            }
        }

        $instanceClassname = $class->getInstanceClassname();
        $factoryMethod = $class->getFactoryClass() && null === $class->getFactoryMethod() ? 'auto' : $class->getFactoryMethod();

        /**
         * Autodiscover for:
         * Factories without method
         * Factories with method == 'auto' and implements FactoryInterface
         * everything with method == 'auto'
         */
        //  $reflectedClass->implementsInterface(FactoryInterface::class)
        if (($class->getFactoryClass() && 'auto' === $factoryMethod) || in_array(FactoryInterface::class, class_implements($instanceClassname))) {
            array_unshift($constructorArguments, $serviceLocator);
            $factoryMethod = 'createService';
        }

        // $reflectedClass->hasMethod($factoryMethod)
        if (null !== $factoryMethod && !method_exists($instanceClassname, $factoryMethod)) {
            throw new Exception\MethodNotFoundException(sprintf('Call to undefined method "%s::%s".',
                $instanceClassname,
                $factoryMethod));
        }

        if (null !== $factoryMethod) {
            /**
             * reduce using this;
             * @see: Zend\Di\Di::createInstanceViaConstructor
             */
            $reflectedClass = new \ReflectionClass($instanceClassname);
            $object = $reflectedClass->getMethod($factoryMethod)->isStatic() ? null : $reflectedClass->newInstance();
            $instance = $reflectedClass->getMethod($factoryMethod)->invokeArgs($object, $constructorArguments);
        } else {
            // Hack to avoid Reflection in most common use cases
            switch (count($constructorArguments)) {
                case 0:
                    $instance = new $instanceClassname();
                    break;
                case 1:
                    $instance = new $instanceClassname($constructorArguments[0]);
                    break;
                case 2:
                    $instance = new $instanceClassname($constructorArguments[0], $constructorArguments[1]);
                    break;
                case 3:
                    $instance = new $instanceClassname($constructorArguments[0], $constructorArguments[1], $constructorArguments[2]);
                    break;
                default:
                    $reflectedClass = new \ReflectionClass($instanceClassname);
                    $instance = $reflectedClass->newInstanceArgs($constructorArguments);
            }
        }

        if ($class->getProperties()) {
            foreach ($class->getProperties() as $property) {
                $method = 'set' . StringUtils::upperCaseWords($property->getName());
                $value = $property->getValue();

                if (!method_exists($instance, $method)) {
                    throw new Exception\MethodNotFoundException(sprintf('Call to undefined method "%s::%s".',
                        get_class($instance), $method));
                }

                if ($property->getType() == ArgumentDefinition::VALUE_TYPE_OBJECT) {
                    $value = $serviceLocator->get($property->getValue());
                }
                call_user_func_array([$instance, $method], [$value]);
            }
        }

        return $instance;
    }
}