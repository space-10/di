<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 21.09.2015
 * Time: 23:49
 */

namespace Space10\Di\Service;


use Space10\Di\Configuration;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ConfigurationFactory
 * @package Space10\Di\Service
 */
class ConfigurationFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $configPaths = [Configuration::DEFAULT_XML_CONFIG_PATH];

        $serviceConfig = $serviceLocator->get('Config');
        if (isset($serviceConfig['space10di']) && isset($serviceConfig['space10di']['config_paths']) && is_array($serviceConfig['space10di']['config_paths'])) {
            error_log('Using space10di config key is deprecated. Use config[space10][di][configuration] instead.', E_USER_DEPRECATED);
            $configPaths = array_merge($configPaths, $serviceConfig['space10di']['config_paths']);
        }

        if (
            isset($serviceConfig['space10']) &&
            isset($serviceConfig['space10']['di']) &&
            isset($serviceConfig['space10']['di']['config_paths']) &&
            is_array($serviceConfig['space10']['di']['config_paths'])
        ) {
            error_log('Using space10[di] config key is deprecated. Use config[space10][di][configuration] instead.', E_USER_DEPRECATED);
            $configPaths = array_merge($configPaths, $serviceConfig['space10']['di']['config_paths']);
        }

        $config = isset($serviceConfig['space10']['di']['configuration']) ? $serviceConfig['space10']['di']['configuration'] : [];
        if (
            isset($serviceConfig['space10']['di']['configuration']['config_paths']) &&
            is_array($serviceConfig['space10']['di']['configuration']['config_paths'])
        ) {
            $configPaths = array_merge($configPaths, $serviceConfig['space10']['di']['configuration']['config_paths']);
        }
        $config['config_paths'] = $configPaths;

        /* @var $moduleManager \Zend\ModuleManager\ModuleManagerInterface */
        $moduleManager = $serviceLocator->get('ModuleManager');
        $configuration = new Configuration($config, $moduleManager);
        return $configuration;
    }
}