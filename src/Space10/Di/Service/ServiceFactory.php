<?php
namespace Space10\Di\Service;

use Space10\Di\ServiceConfig;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ServiceFactory
 * @package Space10\Di\Service
 */
class ServiceFactory implements AbstractFactoryInterface
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceManager;

    protected $loading = false;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @param                         $name
     * @param                         $requestedName
     *
     * @return bool
     */
    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        if (null === $this->serviceManager && !$this->loading) {
            /* @var $serviceManager \Zend\ServiceManager\ServiceManager */
            /* @var $config \Space10\Di\ServiceConfig */

            $serviceManager = $serviceLocator->get('ServiceManager');
            $this->serviceManager = $serviceManager;

            $this->loading = true;
            $config = $serviceLocator->get(ServiceConfig::class);
            $config->configureServiceManager($serviceManager);

            $this->loading = false;
        }

        return $this->serviceManager->has($name);
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @param                         $name
     * @param                         $requestedName
     *
     * @return mixed
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return $this->serviceManager->get($name);
    }
}
