<?php
namespace Space10\Di\Exception;

class DuplicatedIndexException extends InvalidArgumentException
{
}
