<?php
namespace Space10\Di\Exception;

class InvalidTypeException extends InvalidArgumentException
{
}
