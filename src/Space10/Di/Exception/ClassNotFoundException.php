<?php
namespace Space10\Di\Exception;

use DomainException;

class ClassNotFoundException extends DomainException implements ExceptionInterface
{
}
