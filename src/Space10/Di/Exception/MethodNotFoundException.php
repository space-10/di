<?php
namespace Space10\Di\Exception;

class MethodNotFoundException extends InvalidArgumentException
{
}
