<?php
/**
 * Created by PhpStorm.
 * User: Tobias Trozowski
 * Date: 20.09.2015
 * Time: 16:14
 */

namespace Space10\Di;

use Space10\Di\Definition\ClassDefinition;
use Space10\Di\Service\AbstractDiFactoryFactory;
use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\ServiceManager;

/**
 * Class NewServiceConfig
 * @package Space10\Di
 */
class ServiceConfig implements ConfigInterface
{
    const DEFAULT_XML_CONFIG_PATH = 'config/di.xml';

    /**
     * @var ClassDefinition[]
     */
    protected $classes;

    public function __construct(array $classes = [])
    {
        $this->classes = $classes;

        /**
         * load every xml
         * transform each file to array
         * cache it
         *
         */
    }


    /**
     * Configure service manager
     *
     * @param ServiceManager $serviceManager
     * @return void
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {

        foreach ($this->classes as $name => $c) {

            $class = new ClassDefinition($name, $c);

            if ($class->isAbstract()) {
                continue;
            }

            if (!$serviceManager->has($class->getClassname())) {
                if ($class->isInvokable() && $class->isInitializer()) {
                    $serviceManager->addInitializer($class->getClassname());
                } else if ($class->isInvokable()) {
                    $serviceManager->setInvokableClass($class->getClassname(), $class->getClassname());
                } else if ($class->isAbstractFactory() && !$class->getProperties() && !$class->getConstructor()) {
                    $serviceManager->addAbstractFactory($class->getClassname());
                } elseif ($class->isSimpleFactory()) {
                    $serviceManager->setFactory($class->getClassname(), $class->getFactoryClass());
                } else {
                    $factoryFactory = new AbstractDiFactoryFactory($class);
                    $serviceManager->setFactory($class->getClassname(), $factoryFactory);
                }
            }

            // abstract factories cannot be shared(singleton), only the instances they return can be shared (singleton)
            if (!$class->isAbstractFactory() && !$class->isInitializer()) {
                $serviceManager->setShared($class->getClassname(), $class->isSingleton());
            }

            $aliases = $class->getAliases();
//            Debug::dump(debug_backtrace(false,1), $class->getClassname());
            foreach ($aliases as $alias) {
                //Debug::dump($aliases, $class->getClassname());
                $serviceManager->setAlias($alias, $class->getClassname());
            };
        }
    }
}