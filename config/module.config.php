<?php
namespace Space10\Di;

use Space10\Di\Service\ConfigurationFactory;
use Space10\Di\Service\ServiceFactory;

return [
    'service_manager' => [
        'factories'          => [
            ServiceConfig::class => ServiceConfigFactory::class,
            Configuration::class => ConfigurationFactory::class,
        ],
        'abstract_factories' => [
            ServiceFactory::class => ServiceFactory::class,
        ],
    ],
    'space10'         => [
        'di' => [
            'configuration'      => [
                // list of relative paths to the xml di-config
                'config_paths'       => [
                    'etc/di.xml',
                ],
                // config cache instance to use. The retrieved service name will be `Space10\Cache\$thisSetting`
                'config_cache'       => 'Array',
                // store generated factories in filesystem or not
                'generate_factories' => true,
                // directory where factories will be stored. By default this is in the `data` directory of your application
                'factory_dir'        => 'var/cache/space10/factory',
                // namespace for the generated factories
                'factory_namespace'  => 'Space10\Di\Factory',
            ],
        ],
    ],
];
